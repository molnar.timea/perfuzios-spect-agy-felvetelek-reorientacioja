import numpy as np
from scipy.spatial.transform import Rotation as sciRot
import math
import scipy.ndimage as nd
from itertools import product


def get_best_reflection_plane(volume_to_check, base_plane, range_of_rot):
    """
    get the best symmetry plane from range of rotated planes around base plane
    :param volume_to_check: image to calculate symmetry on
    :param base_plane: starter plane
    :param range_of_rot: range of rotations around base plane
    :return: normal vector with max symmetry
    """
    rot_range = [-1*range_of_rot, 0, range_of_rot]
    print('angles to check around current estimate : ' + str(rot_range))
    rotation_angles = list(product(rot_range, repeat=3))
    print('number of rotated planes cross correlation is calculated for : ' + str(len(rotation_angles)))
    rotations = sciRot.from_euler('zyx', rotation_angles, degrees=True)
    rotated_normals = rotations.apply(base_plane)

    cc = [cross_correlation(volume_to_check, v) for v in rotated_normals]
    max = np.argmax(cc)
    best_estimate = rotated_normals[max]
    print('max cc of new rotations : ' + str(cc[max]))
    print('angles rotated from current estimate : ' + str(rotation_angles[max]))
    print('new msp estimate normal : ' + str(best_estimate) + '\n')
    return best_estimate


def cross_correlation(volume, plane_norm, th=0.1):
    """
    :param volume: 3D volume to mirror
    :param plane_norm: normal vector of reflection plane
    :param th: threshold for disregarding voxels with values below
    :return: symmetry measured for the given plane
    """
    # only consider voxels above threshold
    image_coordinates = np.asarray(np.where(volume > th)).T
    # origin of coordinate sys coordinates
    o = np.asarray(volume.shape) / 2
    o = o.astype(int)

    coordinate_sys_coordinates = image_coordinates - o

    # only negative side of the plane:
    relative_pos_to_plane = np.sum(plane_norm * coordinate_sys_coordinates, 1)
    coordinate_sys_coordinates = coordinate_sys_coordinates[relative_pos_to_plane < 0]
    image_coordinates = image_coordinates[relative_pos_to_plane < 0]

    # mirror
    mirrored_image_coordinates = mirror_points(coordinate_sys_coordinates, plane_norm)
    mirrored_image_coordinates = np.around(mirrored_image_coordinates + o).astype(int)

    # only take into account where mirrored coordinates exist on the pictures as well
    is_in_image = (mirrored_image_coordinates[:, 0] >= 0) & (mirrored_image_coordinates[:, 0] < volume.shape[0]) & \
                (mirrored_image_coordinates[:, 1] >= 0) & (mirrored_image_coordinates[:, 1] < volume.shape[1]) & \
                (mirrored_image_coordinates[:, 2] >= 0) & (mirrored_image_coordinates[:, 2] < volume.shape[2])
    image_coordinates = image_coordinates[is_in_image]
    mirrored_image_coordinates = mirrored_image_coordinates[is_in_image]

    indices = tuple(map(tuple, image_coordinates))
    mirrored_indices = tuple(map(tuple, mirrored_image_coordinates))
    f = np.asarray([volume[indices[idx]] for idx in range(len(indices)) if volume[mirrored_indices[idx]] > th ])
    g = np.asarray([volume[mirrored_indices[idx]] for idx in range(len(mirrored_indices)) if volume[mirrored_indices[idx]] > th ])

    if len(f) == 0 or len(g) == 0:
        return 0

    f = f - np.mean(f)
    g = g - np.mean(g)

    cross_corr = np.sum(f * g) / np.linalg.norm(f) * np.linalg.norm(g)

    return cross_corr


def rotate_image_zyx(volume, angles, order=1, reshape=False):
    """
    rotate image with euler angles in zyx order
    """
    rotated = np.copy(volume)
    if angle_check(angles[0]):
        rotated = nd.interpolation.rotate(rotated, angles[0], (0, 1), reshape=reshape, mode='constant', cval=0.0, order=order)
    if angle_check(angles[1]):
        rotated = nd.interpolation.rotate(rotated, angles[1], (0, 2), reshape=reshape, mode='constant', cval=0.0, order=order)
    if angle_check(angles[2]):
        rotated = nd.interpolation.rotate(rotated, angles[2], (1, 2), reshape=reshape, mode='constant', cval=0.0, order=order)

    return rotated


def rotate_image_xyz(volume, angles, order=1, reshape=False):
    """
    rotate image with euler angles in xyz order
    """
    rotated = np.copy(volume)
    if angle_check(angles[0]):
        rotated = nd.interpolation.rotate(rotated, angles[0], (1, 2), reshape=reshape, mode='constant', cval=0.0, order=order)
    if angle_check(angles[1]):
        rotated = nd.interpolation.rotate(rotated, angles[1], (0, 2), reshape=reshape, mode='constant', cval=0.0, order=order)
    if angle_check(angles[2]):
        rotated = nd.interpolation.rotate(rotated, angles[2], (0, 1), reshape=reshape, mode='constant', cval=0.0, order=order)
    return rotated


def angle_check(angle):
    return not (-0.01 < angle < 0.01)


def rotation_for_3d_vectors(v1, v2):
    """
    calc rotation between 2 3d vectors
    :param v1: vector 1
    :param v2: vector 2
    :return: rotation in zyx euler angles
    """
    v1 = (v1 / np.linalg.norm(v1)).reshape(3)
    v2 = (v2 / np.linalg.norm(v2)).reshape(3)
    crossP = np.cross(v1, v2)
    dotP = np.dot(v1, v2)
    cross_norm = np.linalg.norm(crossP)
    kmat = np.array([[0, -crossP[2], crossP[1]], [crossP[2], 0, -crossP[0]], [-crossP[1], crossP[0], 0]])
    rot = np.eye(3) + kmat + kmat.dot(kmat) * ((1 - dotP) / (cross_norm ** 2))
    print('rotation of the vector for checking: '+str(rot.dot(v1)))
    r = sciRot.from_matrix(rot)
    rot = r.as_euler('zyx', degrees=True)
    rot[1] *= -1
    return rot


def rotation_for_2d_vectors(v1, v2):
    """
    get rotation angle between 2 2d vectors
    """
    return np.degrees(math.atan2(v2[0] * v1[1] - v2[1] * v1[0], v2[0] * v1[0] + v2[1] * v1[1]))


def mirror_points(coordinates, plane_norm):
    """
    :param coordinates: numpy array in shape: (n, 3) - should contain coordinates in the defined coord. system
    :param plane_norm: normal vector of the plane of reflection
    :return: (n, 3) numpy array containing the reflected coordinates
    """

    a, b, c = plane_norm
    x = coordinates[:, 0]
    y = coordinates[:, 1]
    z = coordinates[:, 2]
    divider = float(a*a + b*b + c*c)
    k = ((-a*x) - (b*y) - (c*z)) / divider

    x2 = a * k + x
    y2 = b * k + y
    z2 = c * k + z
    x3 = 2 * x2 - x
    y3 = 2 * y2 - y
    z3 = 2 * z2 - z

    return np.asarray([x3, y3, z3]).T


def region_growing(image, seed):
    """
    modified version of this implementation: http://notmatthancock.github.io/2017/10/09/region-growing-wrapping-c.html
    :param image: binary image of partial segmentation
    :param seed: index of the starting place for the method
    :return: binary segmentation image of the grown region from seed, 3d matrix array
    """

    segmented = np.zeros_like(image)
    checked = np.zeros_like(segmented)

    # borders
    top_bounds = np.asarray(image.shape) - 1

    b, seed = seed_check(image, seed, checked, top_bounds)
    print('seed check done')
    if not b:
        return segmented

    segmented[seed] = 1
    checked[seed] = 1
    needs_check = get_neighborhood(seed, checked, top_bounds)

    while len(needs_check) > 0:
        idx = needs_check.pop()
        if checked[idx] == 1:
            continue

        checked[idx] = 1

        if image[idx] == 1:
            segmented[idx] = 1
            needs_check += get_neighborhood(idx, checked, top_bounds)

    return segmented


def get_neighborhood(idx, checked, top_bounds):
    """
    helper function for region_growing, return 6
    :param idx: index of voxel for neighbor search
    :param checked: voxels already visited
    :param top_bounds: bounds of the image
    :return: neighbors of idx, list of indices
    """
    nbhd = []

    x_next = (idx[0] + 1, idx[1], idx[2])
    x_prev = (idx[0] - 1, idx[1], idx[2])
    y_next = (idx[0], idx[1] + 1, idx[2])
    y_prev = (idx[0], idx[1] - 1, idx[2])
    z_next = (idx[0], idx[1], idx[2] + 1)
    z_prev = (idx[0], idx[1], idx[2] - 1)

    next = (x_next, y_next, z_next)
    prev = (x_prev, y_prev, z_prev)

    for i, point in enumerate(next):
        if point[i] <= top_bounds[i] and checked[point] == 0:
            nbhd.append(point)

    for i, point in enumerate(prev):
        if point[i] >= 0 and checked[point] == 0:
            nbhd.append(point)

    return nbhd


def seed_check(image, seed, checked, top_bounds):
    """
    make sure the region growing doesn't start from a bad seed
    """
    n = get_neighborhood(seed, checked, top_bounds)
    if len(n) == 0:
        print('Segmentation error')
        return False, seed


    # go through window:
    window = np.arange(10, -10, -1)
    x = [(seed[0] + w, seed[1], seed[2]) for w in window]
    y = [(seed[0], seed[1] + w, seed[2]) for w in window]
    z = [(seed[0], seed[1], seed[2] + w) for w in window]
    for v in x + y + z:
        n = get_neighborhood(v, checked, top_bounds)
        b = all([image[idx] == 0 for idx in n])
        if image[v] != 0 and not b:
            print('new seed')
            return True, v

    print('nothing near the seed')
    return False, seed