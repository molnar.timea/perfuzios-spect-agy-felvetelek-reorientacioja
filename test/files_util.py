import os
import numpy as np
from BrainVolume import BrainVolume
import pyvista as pv

pv.set_plot_theme('night')
np.set_printoptions(suppress=True)

class TestFiles:
    def __init__(self, root="./test_files/"):
        self.dir = root
        self.filePaths = [os.path.join(self.dir, f)
                          for f in os.listdir(self.dir)
                          if os.path.isfile(os.path.join(self.dir, f))]

    def get_sample(self, idx=-1):
        return  BrainVolume(self.filePaths[idx])

    def get_all_files(self):
        return [BrainVolume(path) for path in self.filePaths]


def list_files(root):
    files = []
    for root, directories, file_names in os.walk(root):
        for filename in file_names:
            files.append(os.path.join(root, filename))
    return files


def plot_with_plane(volume, plane_normal, plotter):

    plotter.add_axes()
    plotter.show_grid()

    plotter.add_volume(volume)

    midpoint = np.asarray(volume.shape) / 2

    arrow = pv.Arrow(midpoint, plane_normal, scale=75, shaft_radius=0.5 / 100, tip_radius=1 / 100)
    plotter.add_mesh(arrow, color='yellow')

    plane = pv.Plane(midpoint, plane_normal, i_size=100, j_size=100)
    plotter.add_mesh(plane, color='yellow')