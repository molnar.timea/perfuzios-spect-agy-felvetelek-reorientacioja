import numpy as np
import matplotlib.pyplot as plt

reorient = np.load('./reorienter_zyx_angles.npy')
rotation = np.load('./rotation_xyz_angles.npy')
time = np.load('./exec_times.npy')
msp_time = np.load('./msp_exec_times.npy')
iters = np.load('./iters.npy')

print('teszt forgatások száma: ' +str(len(rotation)))

rotation = rotation[:, ::-1]
rotation = rotation * -1

angle_diff = np.abs(rotation - reorient)

mean_angle_diff = np.mean(angle_diff, 1)

s = 6
percentage = (msp_time / time)*100
print('MSP keresés átlagos ideje: {}%'.format(np.mean(percentage)))

plt.scatter(time, msp_time/10, label='MSP igazítás ideje', marker='s', c='b', s=s)
plt.scatter(time, iters, label='MSP igazítás iterációk', marker='x', c='r', s=s)
plt.ylabel('MSP iterációk száma\nigazítás ideje [10 s]')
plt.xlabel('reorientáció ideje [s]')
plt.xticks(np.arange(0, 140, 10))
plt.yticks(np.arange(0,26,2))
plt.title('MSP igazítás')
plt.legend(loc='upper left')
plt.grid()
plt.show()

plt.scatter(time, mean_angle_diff, c='r', s=s)
plt.ylabel('szög különbség')
plt.xlabel('reorientáció ideje [s]')
plt.title('átlagos forgatás szög különbség és reorientációs idő')
plt.show()

counts, bins = np.histogram(mean_angle_diff, bins=70)
plt.hist(bins[:-1], bins, weights=counts, color = "royalblue")
plt.xticks(np.hstack([[0, 2, 4, 6], np.arange(10, 30, 5)]))
plt.yticks(np.arange(0, 65, 5))
plt.ylabel('')
plt.xlabel('szög különbség átlag')
plt.grid()
plt.show()


f, axes = plt.subplots(1, 3, sharex=True,sharey=True)

for ax, diff, title in zip(axes, [angle_diff[:,0], angle_diff[:,1], angle_diff[:,2]], ['z', 'y', 'x']):
  counts, bins = np.histogram(angle_diff[:,0], bins=70)
  ax.hist(bins[:-1], bins, weights=counts, color = "royalblue")
  ax.set_ylabel('')
  ax.set_xlabel('{} forgatás szög különbsége'.format(title))
  ax.set_title(title)
  ax.set_xticks(np.hstack([[0,2,4,6],np.arange(10, 60, 5)]))
  ax.set_yticks(np.arange(0, 130, 5))
  ax.grid()

plt.show()