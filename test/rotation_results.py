import test.files_util as util
import pydicom
from itertools import product
import maths_util
from BrainVolume import BrainVolume as BV
import numpy as np
import time


def rotations():
    files = util.TestFiles('../correctly_oriented_images/')
    rotation_angles = list(product([-10, 10], repeat=3))
    rotation_angles.extend(list(product([-20, 20], repeat=3)))
    print('number of rotations per image : ' + str(len(rotation_angles)))

    rotation_xyz_angles = []
    reorienter_zyx_angles = []
    exec_times = []

    rotation_number = len(rotation_angles)
    file_number = len(files.filePaths)
    num_range = rotation_number * file_number
    for current in range(num_range):

        print('\nrunning test iteration: ' +str(current+1) + '/' + str(num_range))
        print('***************************************************')

        scan = pydicom.dcmread(files.filePaths[current%file_number])
        data = scan.pixel_array
        rot_xyz = rotation_angles[current%rotation_number]
        data = maths_util.rotate_image_xyz(data, rot_xyz, order=0, reshape=True)

        scan.PixelData = data.tostring()
        scan.NumberOfFrames, scan.Rows, scan.Columns = data.shape
        scan.Frames = scan.NumberOfFrames

        scan.save_as('./tmp.dcm')

        start_time = time.time()
        bv = BV('./tmp.dcm')
        reoriented_angles_zyx = bv.reorient()
        end_time = time.time()
        runtime = end_time - start_time

        print('reorientation done in {} seconds'.format(runtime))
        exec_times.append(runtime)


        rotation_xyz_angles.append(rot_xyz)
        print('\noriginal image was rotated with angles in xyz: ' + str(rot_xyz))
        reorienter_zyx_angles.append(reoriented_angles_zyx)
        print('reorienting angles calculated as zyx: ' + str(reoriented_angles_zyx))

        print('***************************************************')

    x = np.asarray(rotation_xyz_angles)
    z = np.asarray(reorienter_zyx_angles)
    t = np.asarray(exec_times)

    np.save('./rotation_xyz_angles.npy', x)
    np.save('./reorienter_zyx_angles.npy', z)
    np.save('./exec_times.npy', t)

if __name__ == '__main__':
    rotations()