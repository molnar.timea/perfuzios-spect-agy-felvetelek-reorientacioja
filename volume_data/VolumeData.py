import pydicom
import numpy as np
import scipy.ndimage as nd


class VolumeData:

    def __init__(self, path):
        self.path = path
        scan = pydicom.read_file(path)

        self.data = scan.pixel_array
        self.slice_thickness = scan.SliceThickness
        self.pixel_spacing = scan.PixelSpacing

    def norm_values(self):
        self.data = self.data + self.data.min()
        self.data = self.data / self.data.max()

    def spatial_norm(self, mode="upper"):
        if self.slice_thickness - self.pixel_spacing[0] > 0.001:
            return

        slice_factor = 1 / (self.pixel_spacing[0] / self.slice_thickness)
        real_resize_factor = [1, 1, slice_factor]

        self.data = nd.interpolation.zoom(self.data, real_resize_factor)
        print(real_resize_factor)

    def low_noise_masking(self):
        hist, bins = np.histogram(self.data.ravel(), bins=256, range=(self.data.min(), self.data.max()))
        th = bins[0]
        for i in range(len(hist) - 2):
            if hist[i + 1] > hist[i]:
                th = bins[i]
                break
        self.data[self.data < th] = 0

    def shift_center_to_o(self, center=None, order=0):
        cm = np.asarray(nd.center_of_mass(self.data), dtype=int) if center is None else center
        o = np.asarray(self.data.shape)/2
        o = o.astype(int)
        self.data = nd.shift(self.data, shift=o - cm, order=order, cval=0.0, mode='constant')